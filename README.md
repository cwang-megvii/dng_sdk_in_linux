### Build `dng_sdk` and `xmp_sdk` in Linux

#### Tested Environment
- Ubuntu 16.04
- GCC 5.4.0

#### Step 1:

Download the code from the following address:

```
git clone https://cwang-megvii@bitbucket.org/cwang-megvii/dng_sdk_in_linux.git
```

#### Step 2:

Build `xmp_sdk`: Go to directory `xmp_sdk/build`, run `make`.
Then you will see lots of warnings, but don't worry, at last it will be okay.

#### Step 3:

Build `dng_sdk`: Go to directory `dng_sdk/source/`, open file `Makefile`.
Specify the value of `XMP_PUB_DIR` in Line 9. It is full path of `xmp_sdk/public`, in which directory there are two sub-directories `include` and `libraries`. After Step 2 is done, directory `xmp_sdk/public/libraries/i80386linux_x64/release` should contain 4 files: 
- `libXMPCore.so`
- `libXMPFiles.so`
- `staticXMPCore.ar`
- `staticXMPFiles.ar`

Then just `make`, you will get output file `dng_validate` in current directory.

#### Step 4:

Copy the two `*.so` files into system path:
```
sudo cp *.so /usr/lib/
```
Now you can run `./dng_validate` in `dng_sdk/source`.

--- 
Enjoy.

Chuan Wang
2018/08/16
